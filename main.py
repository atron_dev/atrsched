#!/usr/bin/env python

#pandrev1.1
import pandas as pd
import time
from sqlalchemy import create_engine
from config import *
from apscheduler.schedulers.background import BackgroundScheduler

scheduler = BackgroundScheduler()
engine = create_engine('postgresql://{}:{}@{}/{}'.format(postgre_user,postgre_password,postgre_hostname,postgre_dbname), echo=False)


def hourly_processing():
    print("Start hourly process")
    global_start = time.time()
    start = time.time()
    pq_get_occ = "SELECT DISTINCT ON (site_id) site_id,current_occ AS hourly_occ,time FROM all_occ where time>=now()-interval'1 hour' ORDER BY site_id, hourly_occ DESC"
    new_hourly_data = pd.read_sql(sql=pq_get_occ,con=engine)
    pq_hourly_occ = "SELECT site_id,hourly_occ,time FROM hourly_occ_current"
    current_hourly_data = pd.read_sql(sql=pq_hourly_occ,con=engine)
    end = time.time()
    print(" Hourly data query elapsed time : {}".format(end-start))

    new_hourly_data['time'] = pd.Timestamp.now()
    new_hourly_data.to_sql('hourly_occ_current',if_exists='replace',con=engine)
    current_hourly_data.to_sql('hourly_occ_log',if_exists='append',con=engine)
    global_stop = time.time()
    print("Processing done, result sent to postgresql")
    print("Elapsed time: ",global_stop-global_start)

def daily_processing():
    print("Start daily process")
    global_start = time.time()

    start = time.time()
    pq_get_occ = "SELECT DISTINCT ON (site_id) site_id,hourly_occ AS daily_occ,time FROM hourly_occ_log where time>=now()-interval'1 day' ORDER BY site_id, daily_occ DESC"
    new_daily_data = pd.read_sql(sql=pq_get_occ,con=engine)

    pq_daily_occ = "SELECT site_id,daily_occ,time FROM daily_occ_current"
    current_daily_data = pd.read_sql(sql=pq_daily_occ,con=engine)
    end = time.time()
    print(" Daily data query elapsed time : {}".format(end-start))

    new_daily_data['time'] = pd.Timestamp.now()

    new_daily_data.to_sql('daily_occ_current',if_exists='replace',con=engine)
    current_daily_data.to_sql('daily_occ_log',if_exists='append',con=engine)

    print(" To SQL Elapsed time : {}".format(end-start))
    global_stop = time.time()
    print("Processing done, result sent to postgresql")
    print("Elapsed time: ",global_stop-global_start)

def weekly_processing():
    print("Start weekly process")
    global_start = time.time()

    pq_get_occ = "SELECT DISTINCT ON (site_id) site_id,daily_occ AS weekly_occ,time FROM daily_occ_log where time>=now()-interval'1 week' ORDER BY site_id, weekly_occ DESC"
    new_weekly_data = pd.read_sql(sql=pq_get_occ,con=engine)

    pq_weekly_occ = "SELECT site_id,weekly_occ,time FROM weekly_occ_current"
    current_weekly_data = pd.read_sql(sql=pq_weekly_occ,con=engine)
    end = time.time()
    print(" Weekly data query elapsed time : {}".format(end-start))

    new_weekly_data['time'] = pd.Timestamp.now()

    new_weekly_data.to_sql('weekly_occ_current',if_exists='replace',con=engine)
    current_weekly_data.to_sql('weekly_occ_log',if_exists='append',con=engine)

    print(" To SQL Elapsed time : {}".format(end-start))
    global_stop = time.time()
    print("Processing done, result sent to postgresql")
    print("Elapsed time: ",global_stop-global_start)

def main():
    job1 = scheduler.add_job(hourly_processing,trigger='cron', hour="*", minute="3")
    job2 = scheduler.add_job(daily_processing,trigger='cron', day="*", minute="0")
    job3 = scheduler.add_job(weekly_processing,trigger='cron', week="*", minute="1")
    scheduler.start()
    while True:
        time.sleep(1)

if __name__ == "__main__":
    main()
